from digi import xbee
from digi.xbee.devices import XBeeDevice

class Receiver():
    
    def __init__(self, xbee):
        self.xbee = xbee

    def my_data_received_callback(self, xbee_message):
        self.address_64bit = xbee_message.remote_device.get_64bit_addr()
        self.data = xbee_message.data.decode("utf8")
        print("Received data from %s: %s" % (self.address_64bit, self.data))

    def AddCallback(self):
        self.xbee.add_data_received_callback(self.my_data_received_callback)
        