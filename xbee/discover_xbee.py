from digi.xbee.devices import XBeeDevice
import time
class Discover():
    def __init__(self, xbee):
       self.xbee = xbee # This allows the local xbee device to be passed to the class when called.

    def callback(self, remote):
        remote.get_64bit_addr()
        remote.get_node_id()
        remote.get_role()

    def get_remote_devices(self):
        xbee_net = self.xbee.get_network()
        xbee_net.add_device_discovered_callback(self.callback)
        xbee_net.set_discovery_timeout(5)
        xbee_net.start_discovery_process()
        while xbee_net.is_discovery_running():
            time.sleep(0.5)
        tempNodes = xbee_net.get_devices()
        

        if len(tempNodes) >= 1:
            self.nodes = tempNodes #This passes a list of nodes, which can be indexed into.
            return self.nodes
        else:
            pass