import sys
import glob
import serial
from time import sleep

from digi.xbee.devices import XBeeDevice

from discover_xbee import Discover
from recieve_xbee import Receiver
from send_xbee import send_message


#Start by initializing the controller
class Controller():

    def __init__(self, port, baud):
        self.port = port
        self.baud = baud

    def open_local_xbee(self):
        #Opening my local xbee_device for use.
        self.xbee = XBeeDevice(self.port, self.baud)
        self.xbee.open()
    
    def enable_discovery(self):
        #this enables the discover class from discover_xbee file and passes the local device to the class.
        self.discover = Discover(self.xbee)

    def enable_send(self):
        #this enables the send class from the send_xbee.
        self.sender = send_message(self.xbee)
        
    def get_local_information(self, xbeedevice):
        print("Local MAC addresses:",xbeedevice.get_64bit_addr())
        print("Local node ID:",xbeedevice.get_node_id())
        print("Local role:",xbeedevice.get_role())

#Here we select the COM_Port and Baudrate for controller on my main desktop pc using code that checks active COMports and indexes them into a list we can chose from.
active_ports = []
if sys.platform.startswith('win'):
    ports = ['COM%s' % (i + 1) for i in range(256)]
elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
    ports = glob.glob('/dev/ttyUSB[A-Za-z0-9]')
elif sys.platform.startswith('darwin'):
    ports = glob.glob('/dev/tty.')
else:
    raise EnvironmentError('Unsupported platform')
for port in ports:
    try:
        s = serial.Serial(port)
        s.close()
        active_ports.append(port)
                
    except (OSError, serial.SerialException):
        pass
    
#In this section you can see me calling all the different classes from the different files so that we can use them within our controller.
print(active_ports)
master = Controller(active_ports[int(input("Please Select COMport: "))],9600)
master.open_local_xbee()

recieve = Receiver(master.xbee)

master.get_local_information(master.xbee)

master.enable_discovery() #I call discover to instaniate the class. This passes local xbee to the class
master.discover.get_remote_devices()

master.enable_send()

#Loop for the "menu" to send messages to the different devices

try:
    for idx, device in enumerate(master.discover.nodes):
            print("Remote Device discovered:", device, ":", idx)

except Exception as e:
    print(e)

module = int(input("Choose a node to send to\n"))
remote_device = master.discover.nodes[module]

recieve.AddCallback()

while True:
    msgOut = input("Write message here: \n")
    master.sender.sendIt(remote_device, msgOut)
